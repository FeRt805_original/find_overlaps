#!/usr/bin/env python3
import sys
import argparse
import os 
import ipaddress

def createParser ():
    '''Для параметров'''
    parser = argparse.ArgumentParser(prog = 'python3 find_overlaps.py',
            description = 'Программа для поиска пересечений сетей Пример использования: python3 find_overlaps.py --path /home/z/Рабочий\ стол/nets.txt \nПо умолчанию программа ищет файл nets.txt(файл с бренчами) в своей же папке',
            epilog = "\033[41mНужно создать инпут-файл(nets.txt) для запуска\033[0m, где сети разделены переносом строки, а бренчи двойным переносом строки.\nНачинать ввод сетей нужно с первой строки, с первого бренча. Если ничего не понятно, можно использовать аргумент запуска -m (--make), чтобы создать рабочий пример инпут-файла"
            )
    #argparse.FileType()
    parser.add_argument ('-p', '--path', type = str, default='./nets.txt', help = "Путь файла с сетями")
    parser.add_argument("-m", "--make_file", type=str, nargs='?',
                        const=True, default=False, 
                        help="Создать файл со стандратными бренчами для теста. Изначальный путь: ./nets.txt, можно поменять, используя --path")
    return parser

def make_an_object(net_ip):
    ''' Проверка сети на валидность, если нерабочая, то помечаем &'''
    try:
        return ipaddress.IPv4Network(net_ip)

    except ValueError:
        return "&" + net_ip
    
def get_nets(namespace):
    '''Читает файл с сетями'''
    try:
        with open(namespace.path,"r") as f:
            l = f.read().split("\n")
    except FileNotFoundError:
    	
    	print("\033[31mИнпут-файл не найден.\033[0m Создайте его или попробуйте опцию -m (--make) для создания рабочего примера инпут файла")
    	sys.exit()
    return l
'''
def check_same(al):
    same = []
    for i in al:

        q = 0
        for y in al:
            if y == i:
                same.append(i)
                q += 1
        same.pop(0)
    if q > 1:
        print("There is a coloboration")

    return same
'''
def make_branches(l):
    '''Создает и возвращает словарь, содержаший бренчи и их значения'''
    #l = get_nets()

    # удаление возможных пустых символов на конце
    while l[-1] == "":
        l.pop()
        
    a = [i for i in l if i != ""]
    
    al = []
    branches = {}
    same = []
#    the_same_for_branches = []
#    the_same_for_nets = [] 
    the_same_nets = []
    #branch = ""
    for i in a:
        q = i.split("\t")
        
        if len(q) == 2 and q[0] != "":
            #print(q)
            branch = q[0]
            branches[branch] = [q[1]]
            
            #al.append(q[1])
            
            #same.append([q[1], branch])
            #print(q, "----q")
            #print(branch)
            #print(al, "----al")
        if q[1] in al:
            for x in same:
                if x[0] == q[1]:
                    print(x[1],"(",q[1], end = " ) ")
                    #pass
                    print("также входит в", branch)
                    #the_same_for_branches.append()
                    the_same_nets.append(q[1])

        #if len(q) == 2 and q[0] == "":
        al.append(q[1])
        if q[0] == "":
            branches[branch].append(q[1])
        same.append([q[1], branch])
    #print(branches)
    #same = check_same(al)
    #print(same)
    return branches, al, the_same_nets

def what_branch_dict_generator(branches, al):
    '''Функция для выяснения какому бренчу принадлежит сеть'''
    l = {i: "" for i in al}
    for net in al:
        for branch in branches.keys():
            for i in branches[branch]:
                if i == net:
                    l[net] = branch

    return l
    
def parametr_control():
    parser = createParser()
    namespace = parser.parse_args (sys.argv[1:])
 
    text = '''Branch_1	10.10.0.0/22
	10.100.0.0/16
	192.168.50.0/24
	172.16.0.0/30
	1.0.0.0/29
Branch_2	10.10.1.0/24
	192.168.50.128/26
	1.0.0.8/29
Branch_3	10.101.0.0/16
	192.168.50.64/26
	192.168.48.0/21
	192.168.40.0/21


'''
    if namespace.make_file:
        if os.path.exists(namespace.path):
            with open(str(namespace.path), "w") as f:
                print("\033[32mFile sucessfully changed\033[0m")
                print("Here is it: " + os.path.abspath(namespace.path + "\n"))
                f.write(text)
        else:
            with open(str(namespace.path), "x") as f:
                print("\033[32mFile sucessfully made\033[0m")
                print("Here is it: " + os.path.abspath(namespace.path) + "\n")
                f.write(text)

        f.close()

    return namespace

def what_branch(ip, branches):
    #print(branches.items())
    q = ""
    for i in branches.items():
        for y in i[1]:
            if y == ip:
                q += i[0]
                q += " и "

    return q[:-2]
    #for branch in branches.keys():
    #    for i in branches[branch]:


if __name__ == '__main__':

    ns = parametr_control()


    branches, al, the_same_nets = make_branches(get_nets(ns))
    #branches = словарь со всеми сетями, где ключи - название бренча
    #al = все сети в одном списке
    
    #what_branch = what_branch_dict_generator(branches, al) # Словарь, типа ip:branch
    #print(what_branch(al[2],branches))
    
    _nets = [make_an_object(i) for i in al] # получаем список объктов

    nets = [] # Список из объектов, но без ошибок
    #nets2 = reversed(nets.copy)
    ERROR = False
    errors = [] # Здесь покоятся невозможные, но заданные сети
    for i in _nets:
        
        if i[0] == "&":
            ERROR = True
            errors.append(i[1:])
            
        else:
            nets.append(i)
            

    overlaps = {} # Все пересечения 

    # Добавляем в словарь overlaps пересекающиеся сети сеть: [сабсеть, сабсеть, сабсеть]

    for i in nets:
        #nets.remove(i)
        overlap = False
        overlaps[str(i)] = []
        for y in nets:
            if i.overlaps(y):
                overlap = True
                overlaps[str(i)].append(str(y))
    #print(overlaps)            
    # Сети содержат самих себя в себе, поэтому  удаляем эту проблему из overlaps
    # Теперь удалеем все одинаковые сети
    removing = True
    while 1:
        removing = False
        for i in overlaps.keys():
            for y in overlaps[i]:
                #TODO Удаляется повтор не себя 
                if y == i:
                    overlaps[i].remove(i)
                    removing = True
        if not removing:
            break
    #print()
    #print(what_branch)
    #print(overlaps)
    
    #print(overlaps)
    # Удаляем повторяющиеся элементы из overlaps
    end = True
    while end:
        for branch in overlaps.keys():
            for i in overlaps[branch]:
                #print(i)
                if overlaps[branch].count(i) >= 2:
                    overlaps[branch].remove(i)
                else:
                    end = False
    
   # print("\n", branches)
    if ERROR:
        print("There is a number of wrong nets. Here are they:")
        for i in errors:
            print(i, "из бранча -", what_branch(i, branches))
        print("")
        
    out = ''
    #print(overlaps)
    for key in overlaps.keys():
        for i in overlaps[key]:
            '''
            bad = False
            for q in the_same_nets:
                if q == key:
                    print("ОДИНАКОВЫЕ СЕТИ")
                    bad = True
            '''
            #for y in overlaps[key]:
            
            #if not bad:
            out += "{0} ( {2} ) пересекает {1} ( {3} )\n".format(what_branch(key, branches), what_branch(i, branches), key, i)
                    
    print(out)
            
